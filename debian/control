Source: golang-github-gopherjs-jsbuiltin
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Alexandre Viau <aviau@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-gopherjs-gopherjs-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-gopherjs-jsbuiltin
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-gopherjs-jsbuiltin.git
Homepage: https://github.com/gopherjs/jsbuiltin
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/gopherjs/jsbuiltin

Package: golang-github-gopherjs-jsbuiltin-dev
Architecture: all
Depends: golang-github-gopherjs-gopherjs-dev,
         ${misc:Depends}
Description: GopherJS bindings for built-in JavaScript functions
 JavaScript has a small number of built-in functions to handle
 some common day-to-day tasks. This package providers wrappers
 around some of these functions for use in GopherJS.
 .
 It is worth noting that in many cases, using Go's equivalent
 functionality (such as that found in the net/url package) may
 be preferable to using this package, and will be a necessity
 any time you wish to share functionality between front-end
 and back-end code.
